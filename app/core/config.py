import os

from pydantic import PostgresDsn, RedisDsn
from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    app_title: str = os.getenv("APP_TITLE")
    app_description: str = os.getenv("APP_DESCRIPTION")

    pg_url: PostgresDsn = os.getenv("PG_URL")
    redis_url: RedisDsn = os.getenv("REDIS_URL")

    jwt_algorithm: str = os.getenv("JWT_ALGORITHM")
    jwt_expire: int = os.getenv("JWT_EXPIRE")


settings = Settings()
