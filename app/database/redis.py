import redis.asyncio as redis

from app.core.config import settings


async def get_redis():
    return await redis.from_url(
        settings.redis_url.unicode_string(),
        encoding="utf-8",
        decode_responses=True
    )
