from collections.abc import AsyncGenerator

from sqlalchemy.ext.asyncio import async_sessionmaker, create_async_engine

from app.core.config import settings
from app.utils.logging import AppLogger

logger = AppLogger.__call__().get_logger()

engine = create_async_engine(
    settings.pg_url.unicode_string(),
    future=True,
    echo=True,
)

AsyncSessionFactory = async_sessionmaker(engine, autoflush=True, expire_on_commit=False)


async def get_db() -> AsyncGenerator:
    async with AsyncSessionFactory() as session:
        yield session
